<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('candidatoslocales','CandidatosLocales');

Route::get('/candidatosFederales', 'indexControlador@CF');

Route::resource('bitacora','Bitacora');

Route::resource('candidatos_inactivos','Candidatos_inactivos');

Route::resource('gestion_de_usuario','Gestion_de_usuario');
