@extends('template_site.template')


@section('content')

<div class="page-head">
        <h2 class="page-head-title">Candidatos Inactivos</h2>
      </div>



      <div class="main-content container-fluid">

                <div class="row">
                    <div class="col-sm-12">

                      <div class="panel panel-default">
                        <div class="tab-container">

                          <div class="tab-content">
                            <div id="PresidenteRepublica" class="tab-pane active cont">

                              <!--inicia Tabla para PR-->
                              <div class="panel panel-default panel-table">
                                <div class="panel-body scrollTabla">
                                    <table id="tablaPresidenteRepublica" class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                                    <thead>
                                        <tr class="center" role="row">
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">ID</th>
                                          <th class="sorting_asc" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 176px;">Nombres</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 220px;">Apellido Paterno</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 200px;">Apellido Materno</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 150px;">Apodo</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Cargo</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Jerarquía</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Localidad</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estado</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Municipio</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Distrito</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estatus</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Alianza</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Descripción Alianza</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Alta</th>
                                          <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Baja</th>
                                        </tr>
                                    </thead>



                                    <tbody>
                                      <tr class="gradeA odd" role="row">
                                          <td class="center">1</td>
                                          <td class="center">Miguel</td>
                                          <td class="sorting_1">Vazquez</td>
                                          <td class="center">Guerrero</td>
                                          <td class="center">Pancho</td>
                                          <td class="center">Diputado</td>
                                          <td class="center">Federal</td>
                                          <td class="center">Santa Cruz</td>
                                          <td class="center">Jalisco</td>
                                          <td class="center">Chalco</td>
                                          <td class="center">Activo</td>
                                          <td class="center">4</td>
                                          <td class="center">Nunguna</td>
                                          <td class="center">-----</td>
                                          <td class="center">15/05/09</td>
                                          <td class="center">15/02/09</td>

                                      </tr>
                                      <tr class="gradeA odd" role="row">
                                          <td class="center">2</td>
                                          <td class="center">Victor</td>
                                          <td class="sorting_1">Francisco</td>
                                          <td class="center">Capo</td>
                                          <td class="center">Rana</td>
                                          <td class="center">Presidente Municipal</td>
                                          <td class="center">Local</td>
                                          <td class="center">San Francisco</td>
                                          <td class="center">colima</td>
                                          <td class="center">Iztapalpa</td>
                                          <td class="center">Inactivo</td>
                                          <td class="center">2</td>
                                          <td class="center">Nunguna</td>
                                          <td class="center">-----</td>
                                          <td class="center">15/05/2009</td>
                                          <td class="center">15/02/2016</td>

                                      </tr>
                                      <tr class="gradeA odd" role="row">
                                          <td class="center">3</td>
                                          <td class="center">Aldo</td>
                                          <td class="sorting_1">Natos</td>
                                          <td class="center">Zanchez</td>
                                          <td class="center">Manolo</td>
                                          <td class="center">Senador</td>
                                          <td class="center">Federal</td>
                                          <td class="center">San Martin</td>
                                          <td class="center">Sinaloa</td>
                                          <td class="center">Chalco</td>
                                          <td class="center">Activo</td>
                                          <td class="center">4</td>
                                          <td class="center">Nunguna</td>
                                          <td class="center">-----</td>
                                          <td class="center">15/05/2017</td>
                                          <td class="center">15/02/2018</td>

                                      </tr>

                                    </tbody>
                                    </table>
                                </div>
                              </div>
                              <!--// finaliza tabla PR-->
                            </div>


                            </div>
                          </div>
                        </div>
                      </div>



                    </div>
                </div>


  </div>

@endsection
