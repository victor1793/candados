@extends('template_site.template')


@section('content')

<div class="page-head">
        <h2 class="page-head-title">Gestión de Usuarios </h2>
      </div>



      <div class="main-content container-fluid">

                <div class="row">
                    <div class="col-sm-12">

                      <div class="panel panel-default">
                        <div class="tab-container">

                          <div class="tab-content">

                              <p><a class="btn btn-success">+ Agregar</a></p>
                              <!--inicia Tabla para PR-->
                              <div class="panel panel-default panel-table">
                                <div class="panel-body scrollTabla">
                                    <table id="tablaPresidenteRepublica" class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Editar</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Baja</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Foto</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 176px;">Nombres</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 220px;">Apellido Paterno</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 200px;">Apellido Materno</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 150px;">Apodo</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Cargo</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Jerarquía</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Localidad</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estado</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Municipio</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Distrito</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estatus</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Alianza</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Descripción Alianza</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Alta</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Baja</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center"><a class="btn btn-warning">Editar</a></td>
                                        <td class="center"><a class="btn btn-danger">Dar de Baja</a></td>
                                        <td class="sorting_1">Gecko</td>
                                        <td class="center">Miguel</td>
                                        <td class="center">Vazquez</td>
                                        <td class="center">Guerrero</td>
                                        <td class="center">Natoz</td>
                                        <td class="center">Presidente Municial</td>
                                        <td class="center">Local</td>
                                        <td class="center">Chalco</td>
                                        <td class="center">Estado de Mexico</td>
                                        <td class="center">-</td>
                                        <td class="center">Activo</td>
                                        <td class="center">-</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                    </tr>

                                    </tbody>
                                    </table>
                                </div>
                              </div>
                              <!--// finaliza tabla PR-->
                            </div>


                            </div>
                          </div>
                        </div>
                      </div>



                    </div>
                </div>


  </div>

@endsection
