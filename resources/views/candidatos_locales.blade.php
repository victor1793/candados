@extends('template_site.template')


@section('content')
  

  <div class="page-head">
          <h2 class="page-head-title">Candidatos Locales</h2>

        </div>
        <div class="main-content container-fluid">
          
          <div class="row">
            
            <div class="col-sm-12">
              <div class="panel panel-default">
                
                <div class="tab-container">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="ui-tabs-accordions.html#gobernador" data-toggle="tab">Gobernador</a></li>
                    <li><a href="ui-tabs-accordions.html#diputadolocal" data-toggle="tab">Presidente Municipal</a></li>
                    <li><a href="ui-tabs-accordions.html#municipio" data-toggle="tab">Diputado Local</a></li>
                  </ul>
                  <div class="tab-content">
                    
                    <div id="gobernador" class="tab-pane active cont">
                      <h4>Gobernadores</h4>



                
                  <table id="table1" class="table table-striped table-hover table-fw-widget">
                    <thead>
                      <tr>
                        <th>Estado</th>
                        <th>Candidato</th>
                        <th>Imagen</th>
                        <th>Fecha de alta</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd gradeX">
                        <td>Pachuca</td>
                        <td>
                          Jaime Rodriguez Calderón
                        </td>
                        <td><img src="{{ asset('assets/img/avatar3.png') }}"></td>
                        <td class="center"><?php //echo date('Y-m-d'); ?></td>
                        
                      </tr>

                      <tr class="even gradeA">
                        <td>Pachuca</td>
                        <td>
                          Jaime Rodriguez Calderón
                        </td>
                        <td><img src="{{ asset('assets/img/avatar3.png') }}"></td>
                        <td class="center"><?php //echo date('Y-m-d'); ?></td>
                        
                      </tr>
                      <tr class="gradeA">
                        <td>Pachuca</td>
                        <td>
                          Jaime Rodriguez Calderón
                        </td>
                        <td><img src="{{ asset('assets/img/avatar3.png') }}"></td>
                        <td class="center"><?php //echo date('Y-m-d'); ?></td>
                        
                      </tr>
                      <tr class="gradeA">
                        <td>Pachuca</td>
                        <td>
                          Jaime Rodriguez Calderón
                        </td>
                        <td><img src="{{ asset('assets/img/avatar3.png') }}"></td>
                        <td class="center"><?php //echo date('Y-m-d'); ?></td>
                        
                      </tr>
                      <tr class="gradeA">
                        <td>Pachuca</td>
                        <td>
                          Jaime Rodriguez Calderón
                        </td>
                        <td><img src="{{ asset('assets/img/avatar3.png') }}"></td>
                        <td class="center"><?php //echo date('Y-m-d'); ?></td>
                        
                      </tr>
                      <tr class="gradeA">
                        <td>Pachuca</td>
                        <td>
                          Jaime Rodriguez Calderón
                        </td>
                        <td><img src="{{ asset('assets/img/avatar3.png') }}"></td>
                        <td class="center"><?php //echo date('Y-m-d'); ?></td>
                        
                      </tr>
                 
                    </tbody>
                  </table>



                    </div>

                    <div id="diputadolocal" class="tab-pane cont">


                      <div class="row">
                         <p><a class="btn btn-success">+ Agregar</a></p>
                              <!--inicia Tabla para PR-->
                              <div class="panel panel-default panel-table">
                                <div class="panel-body scrollTabla">
                                    <table id="tablaPresidenteRepublica" class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Editar</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Baja</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estado</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Municipio</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Foto</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 176px;">Nombres</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 220px;">Apellido Paterno</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 200px;">Apellido Materno</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 150px;">Apodo</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Cargo</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Jerarquía</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Localidad</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estado</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Municipio</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Distrito</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estatus</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Alianza</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Descripción Alianza</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Alta</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Baja</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center"><a class="btn btn-warning">Editar</a></td>
                                        <td class="center"><a class="btn btn-danger">Dar de Baja</a></td>
                                        <td class="sorting_1">Gecko</td>
                                        <td>Firefox 1.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td class="center">1.7</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                    </tr>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center"><a class="btn btn-warning">Editar</a></td>
                                        <td class="center"><a class="btn btn-danger">Dar de Baja</a></td>
                                        <td class="sorting_1">Fanci</td>
                                        <td>Firefox 1.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td class="center">1.7</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                    </tr>
                                    </tbody>
                                    </table>      
                                </div>
                              </div>
                            
                </div>

              </div>
            




                    <div id="municipio" class="tab-pane">
<p><a class="btn btn-success">+ Agregar</a></p>
                              <!--inicia Tabla para PR-->
                              <div class="panel panel-default panel-table">
                                <div class="panel-body scrollTabla">
                                    <table id="tablaSenadores" class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Editar</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Baja</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estado</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Distrito</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Foto</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 176px;">Nombres</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 220px;">Apellido Paterno</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 200px;">Apellido Materno</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 150px;">Apodo</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Cargo</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Jerarquía</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Localidad</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estado</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Municipio</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Distrito</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Estatus</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Alianza</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Descripción Alianza</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Alta</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Fecha Baja</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center"><a class="btn btn-warning">Editar</a></td>
                                        <td class="center"><a class="btn btn-danger">Dar de Baja</a></td>
                                        <td class="sorting_1">Test</td>
                                        <td>Firefox 1.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td class="center">1.7</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                    </tr>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center"><a class="btn btn-warning">Editar</a></td>
                                        <td class="center"><a class="btn btn-danger">Dar de Baja</a></td>
                                        <td class="sorting_1">Prueba</td>
                                        <td>Firefox 1.0</td>
                                        <td>Win 98+ / OSX.2+</td>
                                        <td class="center">1.7</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                        <td class="center">A</td>
                                    </tr>
                                    </tbody>
                                    </table>      
                                </div>
                              </div>

                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
            
        </div>
      




@endsection