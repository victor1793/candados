<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>Candados</title>
    <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/custom.css" type="text/css"/>
  </head>

  <body>
    <div class="be-wrapper be-color-header">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <!-- Top bar -->
  <div class="container-fluid">
          <div class="navbar-header tituloDash">
              <p>@yield('title','Dashboard')</p>
          </div>
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              <li class="dropdown"><a href="index.html#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="assets/img/avatar.png" alt="Avatar"><span class="user-name">Aldo Luna</span></a>
                <ul role="menu" class="dropdown-menu">
                  <li>
                    <div class="user-info">
                      <div class="user-name">{{ Auth::user()->name }}</div>
                      <div class="user-position online">Disponible</div>
                    </div>
                  </li>
                   <li><a href="index.html#"><span class="icon mdi mdi-face"></span>Perfil</a></li>
                  <li> <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }} </form>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="index.html#" class="left-sidebar-toggle">Dashboard</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
              <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                  <li class="parent"><a href=" {{ action('HomeController@index') }} "><img src="{{ asset('assets/img/dashboard_small.png') }}" height="20px" width="15px"><span>Dashboard</span></a></li>
                  <li class="parent"><a href="/candidatosFederales"><img src="{{ asset('assets/img/federales_small.png') }}" height="20px" width="15px"><span>Candidatos Federales</span></a></li>
                  <li class="parent"><a href="{{ action('CandidatosLocales@index') }}"><img src="{{ asset('assets/img/locales_small.png') }}" height="20px" width="15px"><span>Candidatos Locales</span></a></li>
                  <li class="parent"><a href="{{ action('Bitacora@index') }}"><img src="{{ asset('assets/img/bitacora_small.png') }}" height="15px" width="20px"><span>Bitacora</span></a></li>
                  <li class="parent"><a href="{{ action('Candidatos_inactivos@index') }}"><img src="{{ asset('assets/img/inactivos_small.png') }}" height="20px" width="15px"><span>Candidatos Inactivos</span></a></li>
                  <li class="parent"><a href="{{ action('Gestion_de_usuario@index') }}"><img src="{{ asset('assets/img/usuarios_small.png') }}" height="20px" width="15px"><span>Gestión de Usuarios</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="be-content">
        <!-- Main content -->
              <?php //Comienza contenido de vista ?>
              @yield('content')
      </div>
      <div class="be-right-sidebar">
        <!-- Right sidebar -->
      </div>
    </div>

    <!-- INICIA MODAL1-->
    <div id="modalAltaCandidatosFederales" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary" style="display: block; padding-right: 17px;">
      <div class="modal-dialog custom-width">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" modalCerrar="modalAltaCandidatosFederales" class="close md-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title">Form Modal</h3>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label>Email address</label>
              <input type="email" placeholder="username@example.com" class="form-control">
            </div>
            <div class="form-group">
              <label>Your name</label>
              <input type="text" placeholder="John Doe" class="form-control">
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <label>Your birth date</label>
              </div>
            </div>
            <div class="row no-margin-y">
              <div class="form-group col-xs-3">
                <input type="text" placeholder="DD" class="form-control">
              </div>
              <div class="form-group col-xs-3">
                <input type="text" placeholder="MM" class="form-control">
              </div>
              <div class="form-group col-xs-3">
                <input type="text" placeholder="YYYY" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-12">
                <div class="be-checkbox">
                  <input id="check2" type="checkbox">
                  <label for="check2">Send me notifications about new products and services.</label>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
            <button type="button" data-dismiss="modal" class="btn btn-primary md-close">Proceed</button>
          </div>
        </div>
      </div>
    </div>
    <!-- // TERMINA MODAL1-->

    <!-- INICIA BACKGROUND MODAL-->
    <div class="modal-backdrop fade"></div>
    <!-- // TERMINA BACKGROUND MODAL-->

    <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js" type="text/javascript"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
    <script src="{{ asset('assets/js/custom.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
        App.dashboard();

      });
    </script>
  </body>
</html>
