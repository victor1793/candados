@extends('template_site.template')


@section('content')

<div class="page-head">
        <h2 class="page-head-title">Bitacora de Candidatos</h2>
      </div>



      <div class="main-content container-fluid">

                <div class="row">
                    <div class="col-sm-12">

                      <div class="panel panel-default">
                        <div class="tab-container">

                          <div class="tab-content">
                            <div id="PresidenteRepublica" class="tab-pane active cont">

                              <!--inicia Tabla para PR-->
                              <div class="panel panel-default panel-table">
                                <div class="panel-body scrollTabla">
                                    <table id="tablaPresidenteRepublica" class="table table-striped table-hover table-fw-widget dataTable no-footer" role="grid" aria-describedby="table1_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;"># ID</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Usuario</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 111px;">Accción</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 176px;">Descripción</th>
                                            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 220px;">Fecha/Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center">1</td>
                                          <td class="center">skjfsjh020</td>
                                        <td class="sorting_1">Agrago</td>
                                        <td class="center">-</td>
                                        <td class="center">25/09/2016 10:34 am.</td>

                                    </tr>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center">2</td>
                                        <td class="center">ojadisad0q</td>
                                        <td class="sorting_1">Elimino</td>
                                        <td class="center">-</td>
                                        <td class="center">25/09/2017 11:14 pm.</td>

                                    </tr>
                                    <tr class="gradeA odd" role="row">
                                        <td class="center">3</td>
                                        <td class="center">aslksa932</td>
                                        <td class="sorting_1">modifico</td>
                                        <td class="center">-</td>
                                        <td class="ccenter">25/09/2016 10:34 pm.</td>
                                    </tr>
                                    </tbody>
                                    </table>
                                </div>
                              </div>
                              <!--// finaliza tabla PR-->
                            </div>


                            </div>
                          </div>
                        </div>
                      </div>



                    </div>
                </div>


  </div>

@endsection
