﻿$(function () {
    verificarRuta_SeleccionaOpcionMenu();

    $("#modalAltaCandidatosFederales").css("z-index", "-1000");
    $(".modal-backdrop").css("z-index", "-1000");

    $("#tablaPresidenteRepublica").dataTable();
    $("#tablaDiputadosFederales").dataTable();
    $("#tablaSenadores").dataTable();

    // BOTONOES AGREGAR CANDIDATOS
    $("#btnAltaCandidatosFederales").click(function () {
        $("#modalAltaCandidatosFederales").css("z-index", "102");
        $(".modal-backdrop").css("z-index", "101");
        $("#modalAltaCandidatosFederales").addClass("in");
        $(".modal-backdrop").addClass("in");
    });

    // BOTONES CERRAR MODALES
    $("button.close").click(function () {
        $("#" + this.getAttribute("modalCerrar")).removeClass("in");
        $(".modal-backdrop").removeClass("in");
        $("#" + this.getAttribute("modalCerrar")).css("z-index", "-1000");
        $(".modal-backdrop").css("z-index", "-1000");
    });
});

function verificarRuta_SeleccionaOpcionMenu() {
    $(".be-left-sidebar ul.sidebar-elements li").removeClass("active").addClass("parent");
    var i = 0;
    for (; i < $(".be-left-sidebar ul.sidebar-elements li").length; i++){
        if ($(".be-left-sidebar ul.sidebar-elements li:nth-child(" + (i + 1) + ") a").attr("href").search(window.location.pathname) != -1) {
            $(".be-left-sidebar ul.sidebar-elements li").eq(i).removeClass("parent").addClass("active");
        }
    }
}